# OBM Insight

It is a standalone web application based on R Shiny for data exploration of OpenBioMaps databases.

Watch this video tutorial to get started:

Magyarul: https://www.youtube.com/watch?v=_2QWtiHeznM&list=PLD7HfslA2kuIRWMXqjFU1qT5Z-WZ7fe9U

In English: 

In these places you will find running instances that you can work with immediately. You can connect to any known server/database (registered at https://openbiomaps.org/projects/openbiomaps_networks):

http://computation.openbiomaps.org/

http://rshiny.openbiomaps.org/


Have a good journey on your data with OBM-Insight!
